from notation import log
import numpy as np
from scipy.linalg import expm

def logDensity(obs, params, distinguishRemoval, N):
    return getMrecord(obs, params, distinguishRemoval, N, [])[1]

def getMrecord(obs, params, distinguishRemoval, N, recordTimes):
    ''' Recording M^i_t values using a forward slicing traversal.
    Takes as input :
        the observed data decomposed as:
            lista: list of branching times
            listb: list of internal psi-(1-r) along branch nodes
            listc: list of internal psi-(1-r) terminal nodes
            listd: list of internal psi-r terminal nodes
            liste: list of omega-(1-r) events
            listf: list of omega-r events.
        the model parameters:
            lamb, mu, rho, psi, r, omega
        a boolean indicating if we have information on the removal status of samples
            distinguishRemoval
        parameters for the numerical computation:
            N: the maximal size of the ancestral population
            recordTimes: the sequence of times for which we want to compute M^i_t
    And returns :
        recordM: matrix containing the M^i_t values for all values of listt.
        ll: the likelihood of the observation, conditioned on observing at least
            one sampled individual at present.'''

    # Unpacking observations and parameters
    lista, listb, listc, listd, liste, listf = obs
    lamb, mu, rho, psi, r, omega = params
    gamma = lamb + mu + psi + omega
    
    # Sorting all dates from the greatest (which is assumed to be the stem age)
    # to the most recent ones. I.e. forward style.
    allDates = lista + listb + listc + listd + liste + listf + recordTimes + [0]
    allDates = list(set(allDates))      # deleting multiple identical dates
    allDates.sort(reverse=True)

    # Preparing the constant part of the C matrix
    Cbase = gamma*np.diagflat(np.arange(0,N+1)) - lamb*np.diagflat(np.arange(0,N), -1) - mu*np.diagflat(np.arange(1,N+1),1)

    # Initialization
    Mt = np.zeros(N+1)
    Mt[0] = 1
    recordM = np.zeros((len(recordTimes), N+1))
    j = 0               # the index of the recording process
    kt = 1              # current number of lineages on the tree
    
    # Tree slicing into epochs
    th = allDates[0]    # starting date of the epoch
    if th in recordTimes:
        recordM[j,:] = Mt
        j += 1

    for thmoinsun in allDates[1:]:
        
        #print('integration on ', thmoinsun, ',', th, ' with ', kt, ' lineages.')
        # We change the C matrix according to the epoch
        C = Cbase + kt*gamma*np.eye(N+1) - 2*kt*lamb*np.diagflat(np.ones(N),-1)
        deltat = thmoinsun - th
        Mt = np.dot(expm( deltat*C ), Mt)

        if thmoinsun in recordTimes:
            recordM[j,:] = Mt
            j += 1

        # End of the period, there is an observed punctual event:
        if thmoinsun in lista:
            Mt *= lamb
            kt += 1
        elif thmoinsun in listb:
            Mt *= psi * (1-r)
        elif thmoinsun in listc or thmoinsun in listd:
            if distinguishRemoval:
                if thmoinsun in listc:
                    for i in range(N, 0, -1):
                        Mt[i] = psi * (1-r) * Mt[i-1]
                    Mt[0] = 0
                else:
                    Mt *= psi * r
            else:
                for i in range(N, 0, -1):
                    Mt[i] = psi * (1-r) * Mt[i-1] + psi * r * Mt[i]
                Mt[0] = psi * r * Mt[0]
            kt -= 1
        elif thmoinsun in liste or thmoinsun in listf:
            if distinguishRemoval:
                if thmoinsun in liste:
                    for i in range(0, N+1):
                        Mt[i] *= omega * (1-r) * (kt + i)
                else:
                    for i in range(0, N):
                        Mt[i] = omega * r * (i+1) * Mt[i+1]
                    #Mt[N] = 0
            else:
                for i in range(0, N):
                    Mt[i] = omega * r * (i+1) * Mt[i+1] + omega * (1-r) * (kt+i) * Mt[i]
                Mt[N] = omega * (1-r) * (kt+N) * Mt[N]
        elif thmoinsun == 0:
            for i in range(0, N+1):
                Mt[i] *= rho**kt * (1-rho)**i

        print("At time ", thmoinsun, " we have Mt0 =", Mt[0], " Mt1 = ", Mt[1])

        # starting point for next period
        th = thmoinsun

    # The likelihood of the observation conditioned on sampling at least one individual at present
    sumM = sum(Mt)
    if sumM > 0:
        ll = log(sumM) 
    else:
        ll = -1e500

    return recordM, ll


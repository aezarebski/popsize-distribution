from notation import log
import numpy as np
from scipy.linalg import expm

def logDensity(obs, params, distinguishRemoval, N):
    return getLrecord(obs, params, distinguishRemoval, N, [])[1]

def getLrecord(obs, params, distinguishRemoval, N, recordTimes):
    ''' Recording L^i_t values using a backward slicing traversal.
    Takes as input :
        the observed data decomposed as:
            lista: list of branching times
            listb: list of internal psi-(1-r) along branch nodes
            listc: list of internal psi-(1-r) terminal nodes
            listd: list of internal psi-r terminal nodes
            liste: list of omega-(1-r) events
            listf: list of omega-r events.
        the model parameters:
            lamb, mu, rho, psi, r, omega
        a boolean indicating if we have information on the removal status of samples
            distinguishRemoval
        parameters for the numerical computation:
            N: the maximal size of the ancestral population
            recordTimes: the sequence of times for which we want to compute M^i_t
    And returns :
        recordL: matrix containing the L^i_t values for all values of recordTimes.
        ll: the likelihood of the observation, conditioned on observing at least
            one sampled individual at present.'''

    # Unpacking observations and parameters
    lista, listb, listc, listd, liste, listf = obs
    lamb, mu, rho, psi, r, omega = params
    gamma = lamb + mu + psi + omega
    
    # Sorting all dates from the most recent to the greatest 
    # (which is assumed to be the stem age) I.e. backward style.
    allDates = lista + listb + listc + listd + liste + listf + recordTimes + [0]
    allDates = list(set(allDates))      # deleting multiple identical dates
    allDates.sort()

    # Preparing the constant part of the C matrix
    Cbase = -gamma * np.diagflat(np.arange(0, N+1)) + lamb * np.diagflat(np.arange(0, N),1) + mu * np.diagflat(np.arange(1, N+1),-1)

    # Initialization
    recordL = np.zeros((len(recordTimes), N+1))
    j = len(recordTimes)-1                      # the index of the recording process
    kt = len(lista) - len(listc) - len(listd)   # current number of lineages on the tree
    Lt = rho**kt * np.ones(N+1)
    for i in range(0, N+1):
        Lt[i] *= (1-rho)**i

    # Tree slicing backward
    th = 0
    for thplusun in allDates[1:]:
        
        if th in recordTimes:
            recordL[j,:] = Lt
            j -= 1

        # print('integration on ', th, ',', thplusun, ' with ', kt, ' lineages.')
        C = Cbase - kt*gamma*np.eye(N+1) + 2*kt*lamb*np.diagflat(np.ones(N), 1)
        deltat = thplusun - th
        Lt = np.dot(expm( (deltat)*C ), Lt)

        # End of the period, there is an observed punctual event if this is not the root
        if thplusun < allDates[len(allDates)-1]:
            if thplusun in lista:
                Lt *= lamb
                kt -= 1
            elif thplusun in listb:
                Lt *= psi * (1-r)
            elif thplusun in listc or thplusun in listd:
                if distinguishRemoval:
                    if thplusun in listc:
                        for i in range(0, N):
                            Lt[i] = psi * (1-r) * Lt[i+1]
                        #Lt[N] = 0
                    else:
                        Lt *= psi * r
                else:
                    for i in range(0, N):
                        Lt[i] = psi * (1-r) * Lt[i+1] + psi * r * Lt[i]
                    Lt[N] = psi * r * Lt[N]
                kt += 1
            elif thplusun in liste or thplusun in listf:
                if distinguishRemoval:
                    if thplusun in liste:
                        for i in range(0, N+1):
                            Lt[i] *= omega * (1-r) * (kt + i)
                    else:
                        for i in range(N, 0, -1):
                            Lt[i] = omega * r * i * Lt[i-1]
                        Lt[0] = 0
                else:
                    for i in range(N, 0, -1):
                        Lt[i] = omega * (1-r) * (kt+i) * Lt[i] + omega * r * i * Lt[i-1]
                    Lt[0] = omega * (1-r) * kt * Lt[0]
    
        # starting point for next period
        th = thplusun

    if th in recordTimes:
        recordL[j,:] = Lt

    # The likelihood of the observation conditioned on sampling at least one individual at present
    Lroot0 = Lt[0]
    if Lroot0 > 0:
        ll = log(Lroot0) 
    else:
        ll = -1e500

    return recordL, ll



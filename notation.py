from math import log, exp, sqrt


def transformParams(params):
    ''' Takes as input the full list of parameters of the model:
        (lambda, mu, rho, psi, r, omega)
    And outputs the simplified, more interesting parameters for analytical formulas:
        (sqrtDelta, x1, x2) '''
    lamb, mu, rho, psi, r, omega = params
    gamma = lamb + mu + psi + omega
    sqrtDelta = sqrt(gamma**2 - 4*lamb*mu)
    x1 = (gamma - sqrtDelta)/(2*lamb)
    x2 = (gamma + sqrtDelta)/(2*lamb)
    return lamb, sqrtDelta, x1, x2

def utz(interestingParams, t, z):
    ''' Takes as input 
        the interesting parameters (sqrtDelta, x1, x2)
        the time t
        and initial condition z
    And outputs the probability that a process starting with one individual
    at time t goes extinct or unsampled before time 0, with a probability
    to go extinct precisely at present z.'''
    lamb, sqrtDelta, x1, x2 = interestingParams
    numerator = x1*(x2-z) - x2*(x1-z)*exp(-sqrtDelta*t)
    denominator = (x2-z) - (x1-z)*exp(-sqrtDelta*t)
    return numerator/denominator

def Rtz(interestingParams, t, z):
    lamb, sqrtDelta, x1, x2 = interestingParams
    return (sqrtDelta/lamb)**2 * (1/((x2-z) - (x1-z)*exp(-sqrtDelta*t)))**2 * exp(-sqrtDelta*t)

def ptz(interestingParams, t, z):
    ''' Takes as input 
        the interesting parameters (sqrtDelta, x1, x2)
        the time t
        and initial condition z
    And outputs the probability that a process starting with one individual
    at time t leads to one sampled individual at time 0, with a probability
    to go extinct precisely at present z.'''
    return (1-z) * Rtz(interestingParams, t, z)


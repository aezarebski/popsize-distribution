import numpy as np
from notation import log, exp
from drawing import getLtt
import algo1, algo2, algo2b

def getNt(obs, params, distinguishRemoval, N, ntimes):
    ''' Gets the distribution of the number of ancestors in the
    past, knowing (O,T).
    Returns the list of times in the past for which the distribution
    is computed, together with the matrix giving these distribution
    (each line j corresponds to the distribution at time t[j]). '''
    lista, listb, listc, listd, liste, listf = obs
    recordTimes = list(np.linspace(max(lista + listb + listc + listd + liste + listf), 0, ntimes, endpoint=True))

    M, ll = algo2.getMrecord(obs, params, distinguishRemoval, N, recordTimes)
    L, ll = algo1.getLrecord(obs, params, distinguishRemoval, N, recordTimes)
    K = np.zeros((ntimes, N+1))

    for j in range(0, ntimes):
        #print('Just to be sure, the log-likelihood ', ll, ' is ', log(np.dot(M[j,:], L[j,:])), ' at time t=', recordTimes[j])
        K[j,:] = (M[j,:] * L[j,:]) / exp(ll)

    return recordTimes, K


def simNtCondOT(obs, params, distinguishRemoval, N, ntimes):
    ''' Simulates a realization of a trajectory of Nt in the past, knowing (O,T).
    Returns the list of times in the past (tj) together with the trajectory
    (N(tj)). Note that ntimes needs to be huge to increase the accuracy of
    the method.'''
    lamb, mu, rho, psi, r, omega = params
    lista, listb, listc, listd, liste, listf = obs
    maxTime = max(lista + listb + listc + listd + liste + listf)
    recordTimes = list(np.linspace(maxTime, 0, ntimes, endpoint=True))
    dt = maxTime / ntimes

    L, ll = algo1.getLrecord(obs, params, distinguishRemoval, N, recordTimes)
    k, listoftimes = getLtt(obs, recordTimes)
    k.reverse()
    N = np.zeros(ntimes)
    i = 1
    N[0] = i

    state = np.zeros(2)
    target = -np.log( np.random.random(2) )
    for j in range(1, ntimes):
        # update of the current state of the integrals
        state[0] += lamb * (2 * k[j] + i) * L[j,i+1] / L[j,i] * dt
        if i > 0:
            state[1] += mu * i * L[j,i-1] / L[j,i] * dt

        # as soon as a clock is a greater than its target, the
        # event happens and both clocks are re initialized
        if state[0] > target[0]:
            i += 1
            state = np.zeros(2)
            target = -np.log( np.random.random(2) )
        if state[1] > target[1]:
            i -= 1
            state = np.zeros(2)
            target = -np.log( np.random.random(2) )

        # we record the total number of lineages
        N[j] = k[j] + i

    return recordTimes, N

def getQNtUncond(params, t0, ntimes, level):
    ''' Returns the trajectory of the quantile of Nt, only conditioned
    on survival upon time t. Level is the level of the quantile, and
    ntimes is the number of sampled times between 0 and t0.'''
    recordTimes = list(np.linspace(0, t0, ntimes, endpoint=False))
    lamb, mu, rho, psi, r, omega = params
    quantiles = []

    for t in recordTimes:
        z = (lamb - mu - (psi+omega)*r) / (lamb * exp((lamb-mu-(psi+omega)*r)*(t0 - t)) - mu - (psi+omega)*r)
        q = log( 1 - level ) / log( 1 - z )
        quantiles.append(q)

    return recordTimes, quantiles

def getENtUncond(params, t0, ntimes):
    ''' Returns the trajectory of the expectation of Nt, only conditioned
    on survival upon time t. Ntimes is the number of sampled times 
    between 0 and t0. '''
    recordTimes = list(np.linspace(0, t0, ntimes, endpoint=False))
    lamb, mu, rho, psi, r, omega = params
    means = []

    for t in recordTimes:
        m = (lamb * exp((lamb-mu-(psi+omega)*r)*(t0-t)) - mu - (psi+omega)*r) / (lamb - mu - (psi+omega)*r)
        means.append(m)

    return recordTimes, means

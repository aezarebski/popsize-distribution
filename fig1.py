import algo1, algo2, algo2b, withoutOmega
from drawing import *
import matplotlib.pyplot as plt
import numpy as np

######################
# PAPER FIGURE
######################
# This figure aims at comparing outputs of the method
# as compared to others, to show that it is accurate

# First subplot
# Comparison to the density values when omega = 0
lista = [7, 6, 4, 3]
listb = [1]
listc = [5]
listd = [2]
liste, listf = [], []
obs = [lista, listb, listc, listd, liste, listf]
drawObs(obs,100,100,'fig1_a.svg')

# params
mu, rho, psi, r, omega = 1, 0.5, 0.3, 0.2, 0
lambValues = [1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2]
popTrunc = 100
distinguishRemoval = True

# probability density values
llKnown = []
llODE1 = []
for lamb in lambValues:
    params = lamb, mu, rho, psi, r, omega
    llKnown.append( withoutOmega.logDensity( obs, params, distinguishRemoval ) )
    llODE1.append( algo1.logDensity( obs, params, distinguishRemoval, popTrunc) )

plt.subplot(231)
plt.plot( lambValues, llKnown, label='analytical formula ($\omega=0$)' , color='#800000')
plt.plot( lambValues, llODE1, 'H', label='algorithm 1\'' , color='#008080' )
plt.xlabel('$\lambda$', fontsize='xx-large')
plt.ylabel(r'$\ln \mathbb{P}(\mathcal{T})$', fontsize='xx-large')
plt.legend(fontsize='large')

# Second subplot
# Comparison to the density values when r = 1
lista = [7, 6, 4, 3]
listb = []
listc = []
listd = [2]
liste = []
listf = [5, 1]
obs = [lista, listb, listc, listd, liste, listf]
drawObs(obs,100,100,'fig1_b.svg')

# params
mu, rho, psi, r, omega = 1, 0.5, 0.3, 1, 0.6
lambValues = [1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2]
popTrunc = 200
distinguishRemoval = True

# probability density values
llAnkit = [-40.4552507476100, -40.8703238897302, -41.3817906577664, -41.9806102480506, -42.6589107277071, -43.4097480561603, -44.2269395654317, -45.1049441532352, -46.0387725346941, -47.0239173599859]
llODE1 = []
for lamb in lambValues:
    params = lamb, mu, rho, psi, r, omega
    llKnown.append( -30 )
    llODE1.append( algo1.logDensity( obs, params, distinguishRemoval, popTrunc) )

plt.subplot(232)
plt.plot( lambValues, llAnkit, label='analytical formula ($r=1$)' , color='#800000')
plt.plot( lambValues, llODE1, 'H', label='algorithm 1\'', color='#008080' )
plt.xlabel('$\lambda$', fontsize='xx-large')
plt.ylabel(r'$\ln \mathbb{P}(\mathcal{T})$', fontsize='xx-large')
plt.legend(fontsize='large')


# Third subplot
# Cross validation of density values from algo 1' against 2'
lista = [9, 8, 7, 3]
listb = [6]
listc = [4]
listd = [2]
liste = [5]
listf = [1]
obs = [lista, listb, listc, listd, liste, listf]
drawObs(obs,100,100,'fig1_c.svg')

# params
mu, rho, psi, r, omega = 1, 0.5, 0.3, 0.2, 0.6
lambValues = [1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2]
popTrunc = 100
distinguishRemoval = True

# probability density values
llODE1 = []
llODE2 = []
for lamb in lambValues:
    params = lamb, mu, rho, psi, r, omega
    llODE1.append( algo1.logDensity( obs, params, distinguishRemoval, popTrunc) )
    llODE2.append( algo2.logDensity( obs, params, distinguishRemoval, popTrunc) )

plt.subplot(233)
plt.plot( lambValues, llODE1, '-', label='algorithm 1\'' , color='#008080' )
plt.plot( lambValues, llODE2, 'H', label='algorithm 2\'' , color='#008080' )
plt.xlabel('$\lambda$', fontsize='xx-large')
plt.ylabel(r'$\ln \mathbb{P}(\mathcal{T}, \mathcal{O})$', fontsize='xx-large')
plt.legend(fontsize='large')


# second row of subplots
# three quantiles compared to particle filtering, 
# on the same dataset, level 0.2, level 0.5 (median) and 0.8.
lista = [10, 9, 6, 3]
listb = [8]
listc = [7]
listd = [2]
liste = [5]
listf = [1]
obs = [lista, listb, listc, listd, liste, listf]
drawObs(obs,100,100,'fig1_def.svg')

# params
mu, rho, psi, r, omega = 1, 0.1, 0.001, 0.5, 0.001
lambValues = [1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2]
popTrunc = 500
distinguishRemoval = False

# quantile values from the particle filtering, assessed by Tim
q02part = [13, 14, 12, 10, 9, 8, 6, 5, 5, 6]
q05part = [20, 21, 19, 16, 14, 12, 10, 8, 7, 9]
q08part = [30, 31, 27, 24, 20, 17, 14, 12, 11, 9]

# quantile values using our algorithms
q02ode, q05ode, q08ode = [], [], []
for lamb in lambValues:
    params = lamb, mu, rho, psi, r, omega
    L, lll = algo1.getLrecord(obs, params, distinguishRemoval, popTrunc, [4])
    M, llm = algo2.getMrecord(obs, params, distinguishRemoval, popTrunc, [4])
    #print('For lambda = ', lamb, ' we get logl = ', lll, ' or using the Mt approach, logl = ', llm)
    K = M[0,:] * L[0,:] 
    K /= np.dot(M[0,:], L[0,:])

    # the +2 corresponds to the number of observed lineages at time t=4
    qode = getQuantiles(K, [0.2, 0.5, 0.8])
    q02ode.append( qode[0]+2 )
    q05ode.append( qode[1]+2 )
    q08ode.append( qode[2]+2 )

plt.subplot(234)
plt.plot( lambValues, q02part, '-', label='particle filter' , color='#800000')
plt.plot( lambValues, q02ode, 'H', label='algorithms 1 \& 2' , color='#008080' )
plt.xlabel('$\lambda$', fontsize='xx-large')
plt.ylabel('quantile of level 0.2', fontsize='large')
plt.legend(fontsize='large')

plt.subplot(235)
plt.plot( lambValues, q05part, '-', label='particle filter' , color='#800000')
plt.plot( lambValues, q05ode, 'H', label='algorithms 1 \& 2' , color='#008080' )
plt.xlabel('$\lambda$', fontsize='xx-large')
plt.ylabel('median', fontsize='large')
plt.legend(fontsize='large')

plt.subplot(236)
plt.plot( lambValues, q08part, '-', label='particle filter', color='#800000' )
plt.plot( lambValues, q08ode, 'H', label='algorithms 1 \& 2' , color='#008080' )
plt.xlabel('$\lambda$', fontsize='xx-large')
plt.ylabel('quantile of level 0.8', fontsize='large')
plt.legend(fontsize='large')


plt.show()



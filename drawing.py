import matplotlib.pyplot as plt
import numpy as np
plt.rc('text', usetex=True)

def getQuantiles(distrib, alphaList):
    ''' Takes as input a discrete distribution on N, and
    the list of levels of quantiles values to be computed.
    Returns the values of these quantiles, sorted from the
    smallest to the biggest level.
    NB: the quantile with level alpha is here defined as 
    the smallest value such that the cumulative distribution
    function is greater than alpha.'''
    alphaList.sort()
    m = len(alphaList)

    qList = []
    cumsum, j, k = 0, 0, 0

    while j < len(distrib) and cumsum <= alphaList[m-1]:
        cumsum += distrib[j]
        while k < m and cumsum > alphaList[k]:
            qList.append(j)
            k += 1
        j += 1

    if j == len(distrib) and cumsum < 1:
        print('The distribution is not a distribution')

    return qList

def getLtt(obs, recordTimes=[]):
    ''' Takes as input the observed data decomposed as:
        lista: list of branching times
        listb: list of internal psi-(1-r) along branch nodes
        listc: list of internal psi-(1-r) terminal nodes
        listd: list of internal psi-r terminal nodes
        liste: list of omega-(1-r) events
        listf: list of omega-r events.
    and returns the lineage-through-time plot from this.'''
    lista, listb, listc, listd, liste, listf = obs
    n = len(lista) - len(listc) - len(listd)
    relevant = lista + listc + listd + recordTimes + [0]
    relevant = list(set(relevant))
    relevant.sort()
    
    ltttimes, ltt = [], []
    for t in relevant:
        # if we are not given the recording times, we record
        # before and after each change in the number of lineages
        if recordTimes == []:
            ltttimes.append(t)
            ltt.append(n)

        # Two possible types of changes
        if t in lista:
            n -= 1
        if t in listc or t in listd:
            n += 1

        # if we are not given the recording times, ...
        if recordTimes == []:
            ltttimes.append(t)
            ltt.append(n)

        if t in recordTimes and t not in ltttimes:
            ltttimes.append(t)
            ltt.append(n)
    
    # for convention, we return these ordered forward in time
    ltt.reverse()
    ltttimes.reverse()
    return ltt, ltttimes


def drawNt(obs, recordTimes, K, listofNchange = [], listN = []):
    ''' Draws the 95% envelope
    of the number of ancestors in the past.
    The optional listofNchange and listN refer
    to the true N trajectory that we may want to
    superimpose on the inference.'''
    # Gets the number of lineages in the observation
    ltt, ltttimes = getLtt(obs, recordTimes)

    # colors palette
    blue = '#548687'
    pink = '#ff8080'
    green = '#8fbc94'
    inference_color = '#808080'

    # plotting samples
    lista, listb, listc, listd, liste, listf = obs
    #plt.plot([0.5]*len(listb+listc), listb+listc, 'o', label='psi-sampled and non-removed', fillstyle='none', color=blue)
    #plt.plot([0.5]*len(listd), listd, 'o', label='psi-sampled and removed', fillstyle='full', color=blue)
    #plt.plot([0.5]*len(liste), liste, 'o', label='omega-sampled and non-removed', fillstyle='none', color=pink)
    #plt.plot([0.5]*len(listf), listf, 'o', label='omega-sampled and removed', fillstyle='full', color=pink)

    # Drawing the true number of ancestors
    plt.plot(ltt, ltttimes, label='$k_t$', lw=2, color=blue)
    if listN != [] and listofNchange != []:
        plt.plot(listN, listofNchange, label='$I_t$', lw=2, color=green)

    # The 0.025 quantile, the median and 0.975 quantile
    q0, q1, q2 = [], [], []
    for j in range(0, len(recordTimes)):
        qList = getQuantiles(K[j,:], [0.025, 0.5, 0.975])
        #print(qList)
        q0.append(qList[0]+ltt[j])
        q1.append(qList[1]+ltt[j])
        q2.append(qList[2]+ltt[j])

    # Graphs
    #plt.plot(q0, recordTimes, color=inference_color, marker='+', ls='--')
    plt.plot(q1, recordTimes, label='median of $K_t$', color=inference_color)
    #plt.plot(q2, recordTimes, color=inference_color, marker='+', ls='--')
    plt.fill_betweenx(recordTimes, q0, q2, alpha=0.1, color=inference_color, label='95\% envelope of $K_t$')
    plt.xlabel('number of individuals')
    plt.ylabel('time')


def drawObs(obs, width, height, filename):
    ''' Draw in a file a svg representing the observation.
    Note that the tree corresponds only to one of the many
    possible topologies, but we don't care so much about that. '''
    myblue = '#548687'
    myred = '#ff8080'

    lista, listb, listc, listd, liste, listf = obs
    x = len(lista)
    w = len(listd)
    v = len(listb)
    y = len(listc)
    if x <= w + y:
        print("Warning ! Too many leaves as compared to branching events !")
    allDates = lista + listb + listc + listd + liste + listf
    allDates.sort()
    yscale = height / max(allDates)

    with open(filename, "a") as fi:

        # header of a svg file
        fi.write('<?xml version=\"1.0\" standalone=\"no\"?>\n')
        fi.write('<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n\n')
        fi.write('<svg width=\"'+ str(width) +'\" height=\"'+ str(height) +'\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n\n')

        # minimal space between two vertical segments and timeline, 
        # depending on the presence of occurrences
        if liste != [] or listf != []:
            xspace = width / (x + 3)
            # we draw the timeline for occurrences
            xtimeline = xspace
            fi.write('<line x1=\"'+ str(xtimeline) +'\" y1=\"'+ str(0) +'\" x2=\"'+ str(xtimeline) +'\" y2=\"'+ str(height) +'\" style=\"fill:none;stroke:'+ 'black' +';stroke-width:1;\" />\n')
            xcurr = 3*xspace
        else:
            xspace = width / (x + 1)
            xcurr = xspace

        # a list for all current segments of branches
        xbranches = [xcurr]
        tjmoinsun = 0

        for tj in allDates:
            # we extend all current branches
            for xb in xbranches:
                fi.write('<line x1=\"'+ str(xb) +'\" y1=\"'+ str(tjmoinsun*yscale) +'\" x2=\"'+ str(xb) +'\" y2=\"'+ str(tj*yscale) +'\" style=\"fill:none;stroke:'+ myblue +';stroke-width:1;\" />\n')

            if tj == max(allDates):
                fi.write('\n</svg>')
            elif tj in lista:
                if len(xbranches) > 1:
                    # we take two branches at random and join them
                    i = np.random.randint(0,len(xbranches)-1)
                    xb1 = xbranches[i]
                    xb2 = xbranches.pop(i+1)
                    fi.write('<line x1=\"'+ str(xb1) +'\" y1=\"'+ str(tj*yscale) +'\" x2=\"'+ str(xb2) +'\" y2=\"'+ str(tj*yscale) +'\" style=\"fill:none;stroke:'+ myblue +';stroke-width:1;\" />\n')
                    xbranches[i] = (xb1+xb2)/2
                else:
                    # we join the only branch in the list with a present-day leaf
                    xb1 = xbranches[0]
                    xcurr += xspace
                    fi.write('<line x1=\"'+ str(xb1) +'\" y1=\"'+ str(tj*yscale) +'\" x2=\"'+ str(xcurr) +'\" y2=\"'+ str(tj*yscale) +'\" style=\"fill:none;stroke:'+ myblue +';stroke-width:1;\" />\n')
                    fi.write('<line x1=\"'+ str(xcurr) +'\" y1=\"'+ str(0) +'\" x2=\"'+ str(xcurr) +'\" y2=\"'+ str(tj*yscale) +'\" style=\"fill:none;stroke:'+ myblue +';stroke-width:1;\" />\n')
                    xbranches[0] = (xb1+xcurr)/2               
            elif tj in listb:
                # one branch chosen at random gets the sampled ancestor
                i = np.random.randint(0, len(xbranches))
                fi.write('<circle cx=\"'+ str(xbranches[i]) +'\" cy=\"'+ str(tj*yscale) +'\" r=\"2\" style=\"fill:none;stroke:'+ myblue +';stroke-opacity:1;stroke-width:0.5\" />\n')
            elif tj in listc:
                # we add a new branch to the collection
                xcurr += xspace
                xbranches.append(xcurr)
                # and we plot a non-removed psi-sample
                fi.write('<circle cx=\"'+ str(xcurr) +'\" cy=\"'+ str(tj*yscale) +'\" r=\"2\" style=\"fill:none;stroke:'+ myblue +';stroke-opacity:1;stroke-width:0.5\" />\n')
            elif tj in listd:
                # we add a new branch to the collection
                xcurr += xspace
                xbranches.append(xcurr)
                # and we plot a removed psi-sample
                fi.write('<circle cx=\"'+ str(xcurr) +'\" cy=\"'+ str(tj*yscale) +'\" r=\"2\" style=\"fill:'+ myblue +';fill-opacity:1;stroke:'+ myblue +';stroke-opacity:1;stroke-width:0.5\" />\n')
            elif tj in liste:
                # we plot a non-removed omega-sample
                fi.write('<circle cx=\"'+ str(xtimeline) +'\" cy=\"'+ str(tj*yscale) +'\" r=\"2\" style=\"fill:none;stroke:'+ myred +';stroke-opacity:1;stroke-width:0.5\" />\n')
            elif tj in listf:
                # we plot a removed omega-sample
                fi.write('<circle cx=\"'+ str(xtimeline) +'\" cy=\"'+ str(tj*yscale) +'\" r=\"2\" style=\"fill:'+ myred +';fill-opacity:1;stroke:'+ myred +';stroke-opacity:1;stroke-width:0.5\" />\n')

            tjmoinsun = tj


    return 'done'

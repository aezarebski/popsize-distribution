from drawing import *
import simu
import numpy as np
import popsize

###################
# Figure in the paper
###################
# This figure aims at illustrating the use of the distribution of the pop size
# on simulated datasets

nsimusteps = 1e6
N = 200
npoints = 100
distinguishRemoval = True

# First subplot, when psi,omega = 0,0
lamb, mu, rho, psi, r, omega = 1, 0.7, 0.2, 0, 0, 0
params = lamb, mu, rho, psi, r, omega
tor = 10
print('Simulating first dataset')
obs, listoftimes, listN = simu.simObs(params, tor, nsimusteps)
drawObs(obs, 1000, 1000, 'fig2_1a.svg')
print('Computing Kt')
recordTimes, K = popsize.getNt(obs, params, distinguishRemoval, N, npoints)

plt.subplot(311)
drawNt(obs, recordTimes, K, listoftimes, listN)
plt.legend()

# Second subplot, when omega = 0
lamb, mu, rho, psi, r, omega = 1, 0.7, 0.2, 0.5, 0.1, 0
params = lamb, mu, rho, psi, r, omega
tor = 10
print('Simulating second dataset')
obs, listoftimes, listN = simu.simObs(params, tor, nsimusteps)
drawObs(obs, 1000, 1000, 'fig2_2a.svg')
print('Computing Kt')
recordTimes, K = popsize.getNt(obs, params, distinguishRemoval, N, npoints)

plt.subplot(312)
drawNt(obs, recordTimes, K, listoftimes, listN)
plt.legend()

# First subplot, when psi,omega = 0,0
lamb, mu, rho, psi, r, omega = 1, 0.7, 0.2, 0.5, 0.1, 0.3
params = lamb, mu, rho, psi, r, omega
tor = 10
print('Simulating third dataset')
obs, listoftimes, listN = simu.simObs(params, tor, nsimusteps)
drawObs(obs, 1000, 1000, 'fig2_3a.svg')
print('Computing Kt')
recordTimes, K = popsize.getNt(obs, params, distinguishRemoval, N, npoints)

plt.subplot(313)
drawNt(obs, recordTimes, K, listoftimes, listN)
plt.legend()



plt.show()
